package com.parse.starter;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 100485988 on 17-Nov-15.
 */
public class ViewGroup extends AppCompatActivity {

    private ArrayList<String> names = new ArrayList<String>();
    private ArrayList<String> usernames = new ArrayList<String>();
    ListView list;
    TextView groupName, groupCode, priceLimit;
    String thisGroupName, thisGroupCode, thisPriceLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_group);

        //the Start Game Button
        Button commence = (Button) findViewById(R.id.btn_play);
        commence.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ParseQuery query = new ParseQuery("Group");
                query.whereEqualTo("sharecode", thisGroupCode);
                query.findInBackground(setCheck);

            }
        });

        //the listview
        list = (ListView) findViewById(R.id.list_players);

        //the passed in values
        Intent i = this.getIntent();
        thisGroupName = i.getStringExtra("groupName");
        thisGroupCode = i.getStringExtra("groupCode");

        //the fields with values pulled from intent data
        groupName = (TextView) findViewById(R.id.value_group_name);
        groupCode = (TextView) findViewById(R.id.value_group_code);
        priceLimit = (TextView) findViewById(R.id.value_price_limit);

        //set the first two textviews
        groupName.setText(thisGroupName);
        groupCode.setText(thisGroupCode);

        //query the online table "UserGroup" for the list of names
        ParseQuery query = new ParseQuery("UserGroup");
        query.whereEqualTo("group", thisGroupCode);
        query.findInBackground(findCheck);

        //query the online table "Group" for the price limit
        ParseQuery query2 = new ParseQuery("Group");
        query2.whereEqualTo("sharecode", thisGroupCode);
        try {
            List<ParseObject> s = query2.find();
            priceLimit.setText(String.valueOf(s.get(0).getInt("pricelimit")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Callback function for findInBackground
    protected FindCallback findCheck = new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> objects, ParseException e) {
            if (e==null) {
                for (ParseObject obj: objects) {
                    names.add(obj.getString("name"));
                    usernames.add(obj.getString("username"));
                }
                populateList(list, names);
            }
            else {

            }
        }
    };
    //Callback function for findInBackground
    protected FindCallback setCheck = new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> objects, ParseException e) {
            if (e==null) {
                objects.get(0).put("ready", true);
                try{objects.get(0).save();}catch (ParseException ex) {}
                //send out push notification
                for (int i=0;i<usernames.size();i++) {
                    ParsePush parsePush = new ParsePush();
                    ParseQuery pQuery = ParseInstallation.getQuery();
                    pQuery.whereEqualTo("username", usernames.get(i));
                    parsePush.sendMessageInBackground(getString(R.string.push_invite), pQuery);
                }
                Sound aria = new Sound(getApplicationContext(), "aria.wav");
                aria.play(0.5f);
            }
            else {

            }
        }
    };

    private void populateList(ListView listView, ArrayList<String> array) {

        listView.setAdapter(new LabelAdapter(this, array));
    }
}