package com.parse.starter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by 100490371 on 11/19/2015.
 */

class Present
{

    private FloatBuffer vertexBuffer;
    private FloatBuffer textureBuffer;

    private int vPos;
    private int TexCoord;
    private int Tex;
    private int Texture;
    private int triangles;

    private float x, y, z;
    private float scalex, scaley;
    private float AlphaChange;

    private float[] augmentPresent = new float[16];

    static float vertexes[] = {
            //one half
            0.0f,0.0f,0.0f,
            1.0f,0.0f,0.0f,
            1.0f,1.0f,0.0f,
            //second half
            0.0f,0.0f,0.0f,
            1.0f,1.0f,0.0f,
            0.0f,1.0f,0.0f,
    };

    static float textureCoord[] = {
            //one half
            0.0f,1.0f,
            1.0f,1.0f,
            1.0f,0.0f,
            //second half
            0.0f,1.0f,
            1.0f,0.0f,
            0.0f,0.0f
    };

    private void SetVertexBuffer()
    {
        ByteBuffer bbv = ByteBuffer.allocateDirect(vertexes.length * 4);//4 is size of float
        bbv.order(ByteOrder.nativeOrder());
        vertexBuffer = bbv.asFloatBuffer();
        vertexBuffer.put(vertexes);
        bbv.position(0);
        vertexBuffer.position(0);
    }

    private void SetTextureBuffer()
    {
        ByteBuffer bbt = ByteBuffer.allocateDirect(textureCoord.length * 4);//4 is size of float
        bbt.order(ByteOrder.nativeOrder());
        textureBuffer = bbt.asFloatBuffer();
        textureBuffer.put(textureCoord);
        bbt.position(0);
        textureBuffer.position(0);
    }

    public Present(float x, float y, int resourseId, Context con)
    {
        SetTextureBuffer();
        SetVertexBuffer();

        triangles = vertexes.length/3;
        Texture = loadTexture(con, resourseId);

        this.x = x;
        this.y = y;
        this.z = 0;
        scalex = scaley = 1;
        AlphaChange = 0;
    }

    public Present(float x, float y, String name, Context con)
    {
        SetTextureBuffer();
        SetVertexBuffer();

        triangles = vertexes.length/3;
        Texture = loadTexture(con, name);

        this.x = x;
        this.y = y;
        this.z = 0;
        scalex = scaley = 1;
        AlphaChange = 0;
    }

    public void draw(int program)
    {
        Matrix.setIdentityM(augmentPresent, 0);
        Matrix.translateM(augmentPresent, 0, x, y, z);
        Matrix.scaleM(augmentPresent, 0, scalex, scaley, 1);

        // Pass the projection and view transformation to the shader
        int location = GLES20.glGetUniformLocation(program, "Location");
        GLES20.glUniformMatrix4fv(location, 1, false, augmentPresent, 0);
        int alpha = GLES20.glGetUniformLocation(program, "alpha");
        GLES20.glUniform1f(alpha, AlphaChange);


        //Load vertex points to vertex shader
        vPos = GLES20.glGetAttribLocation(program, "vPosition");

        GLES20.glVertexAttribPointer(vPos, 3, GLES20.GL_FLOAT, false, 3 * 4, vertexBuffer);

        GLES20.glEnableVertexAttribArray(vPos);


        //Load texture points to vertex shader
        TexCoord = GLES20.glGetAttribLocation(program, "vTex");
        GLES20.glVertexAttribPointer(TexCoord, 2, GLES20.GL_FLOAT, false, 2 * 4, textureBuffer);
        GLES20.glEnableVertexAttribArray(TexCoord);

        //push texture to gpu
        Tex = GLES20.glGetUniformLocation(program, "Tex");
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, Texture);
        GLES20.glUniform1i(Tex, 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, triangles);

        GLES20.glDisableVertexAttribArray(vPos);
        GLES20.glDisableVertexAttribArray(TexCoord);
    }

    public static int loadTexture(final Context context, final String name)
    {

        TextView textView = new TextView(context);

        textView.setText(name);
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        textView.setTextColor(0xffb3d3e9);//61a8da);

        textView.setTextSize(8);
        textView.setDrawingCacheEnabled(true);
        textView.layout(0, 0, 225, 200);

        textView.buildDrawingCache();




        Bitmap image = Bitmap.createBitmap(textView.getDrawingCache());//Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_8888);//

        textView.setDrawingCacheEnabled(false);
//        Canvas c = new Canvas(image);
////        Paint temp = new Paint();
////        temp.setStyle(Paint.Style.FILL);
////        temp.setColor(0xff000000);
////        c.drawPaint(temp);
//
//        Paint text = new Paint();
//
//
//        text.setTextSize(12);
//        text.setColor(Color.RED);
//        c.drawText(name, 0,0,text);


        final int[] textureHandle = new int[1];
        GLES20.glGenTextures(1, textureHandle, 0);

        if(textureHandle[0] != 0)
        {
            //final BitmapFactory.Options opt = new BitmapFactory.Options();
            //opt.inScaled = false;

            //final Bitmap image = BitmapFactory.decodeResource(context.getResources(), resourseId, opt);
            Log.d("Testing Name:" + name, Integer.toHexString(image.getPixel(0, 0)));//String.format("%02X", image.getPixel(0,0)));

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_REPEAT);
            GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_REPEAT);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, image, 0);

//            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(image.getWidth() * image.getHeight() * 4);
//            byteBuffer.order(ByteOrder.BIG_ENDIAN);
//            IntBuffer ib = byteBuffer.asIntBuffer();
//
//            int[] pixels = new int[image.getWidth() * image.getHeight()];
//            image.getPixels(pixels, 0, image.getWidth(), 0, 0, image.getWidth(), image.getHeight());
//            for(int i=0; i<pixels.length; i++){
//                ib.put(pixels[i] << 8 | pixels[i] >>> 24);
//            }
//
//            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);
//
//            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, image.getWidth(), image.getHeight(), 0,
//                    GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, byteBuffer);
//
//            GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR );
//            GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR );
//            GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE );
//            GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE );

            image.recycle();
        }
        else
            throw new RuntimeException("Error loading texture.");


        return textureHandle[0];
    }


    public static int loadTexture(final Context context, final int resourseId)
    {
        final int[] textureHandle = new int[1];
        GLES20.glGenTextures(1, textureHandle, 0);

        if(textureHandle[0] != 0)
        {
            final BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inScaled = false;

            final Bitmap image = BitmapFactory.decodeResource(context.getResources(), resourseId, opt);
            Log.d("Testing", Integer.toHexString(image.getPixel(0, 0)));//String.format("%02X", image.getPixel(0,0)));

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, image, 0);

//            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(image.getWidth() * image.getHeight() * 4);
//            byteBuffer.order(ByteOrder.BIG_ENDIAN);
//            IntBuffer ib = byteBuffer.asIntBuffer();
//
//            int[] pixels = new int[image.getWidth() * image.getHeight()];
//            image.getPixels(pixels, 0, image.getWidth(), 0, 0, image.getWidth(), image.getHeight());
//            for(int i=0; i<pixels.length; i++){
//                ib.put(pixels[i] << 8 | pixels[i] >>> 24);
//            }
//
//            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[0]);
//
//            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, image.getWidth(), image.getHeight(), 0,
//                    GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, byteBuffer);
//
//            GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR );
//            GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR );
//            GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE );
//            GLES20.glTexParameteri ( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE );

            image.recycle();
        }
        else
            throw new RuntimeException("Error loading texture.");


        return textureHandle[0];
    }


    public void SetPresentLocation(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public void ChangeAlpha(float AlphaDecrease)
    {
        this.AlphaChange -= AlphaDecrease;
    }

    public void ScalePresent(float x, float y) {
        this.scalex = x;
        this.scaley = y;
    }

    public void SetPresentForBackGround(float x, float y)
    {
        scalex = x;
        scaley = y;

        this.x = -1*(x/2);
        this.y = -1*(y/2);
    }

    public void setDepth(float z)
    {
        this.z = z;
    }


    public float getX()
    {
        return x;
    }

    public float getY()
    {
        return y;
    }
}


class PackagePresent
{
    public Present p;
    public float angle;

    public PackagePresent(Present pre, float f)
    {
        p = pre;
        angle = f;
    }

    public void draw(int program)
    {
        p.draw(program);
    }
}