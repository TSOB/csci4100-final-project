package com.parse.starter;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by 100490371 on 11/29/2015.
 * User
 * Group
 * UserGroup
 */

@ParseClassName("Person")
public class SantaPerson extends ParseObject {

    public String getUsername()
    {
        return getString("username");
    }

    public String getname()
    {
        return getString("name");
    }

    public static ParseQuery<SantaPerson> getQuery()
    {
        return ParseQuery.getQuery(SantaPerson.class);
    }
}