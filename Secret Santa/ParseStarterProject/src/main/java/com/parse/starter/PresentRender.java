/**
 * Created by 100490371.
 */
package com.parse.starter;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

enum Draw
{
    DrawPresent,
    BrightScreen,
    DeadPresent
}

class PresentRender implements GLSurfaceView.Renderer {

    private final String TAG = "FinalCSCI4100";
    private final String DisplayText = "Have Fun Shopping For:";
    String name;
    Context activity;
    ArrayList<PackagePresent> Presents;
    Runnable MovmentThreadCode;
    Thread MovmentThread = null;
    Present OpenedPresent;
    Present TextPresent;
    Present PresentName;
    Present Background;
    Draw Current_Drawing = Draw.DrawPresent;

    private final float[] MVPMatrix = new float[16];
    private final float[] ProjectionMatrix = new float[16];
    private final float[] ViewMatrix = new float[16];

    private boolean stop = false;
    private boolean pause = false;
    private boolean clicked = false;
    private int slowDown = 0;


    //private final int SlowDownStop = 100;
    private float pace = -0.03f;
    private final int waittime = 10;
    private final float center_x = -0.3f, center_y = -1.9f, center_z = -1.9f, radius_x = 1.9f, radius_y = 1.3f, radius_z = 3f;
    private int program;

    public PresentRender(Context context, String name, boolean picked)
    {
        super();
        this.activity = context;
        this.name = name;
        if(picked)
            Current_Drawing = Draw.BrightScreen;
        else
            Current_Drawing = Draw.DrawPresent;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(1, 1, 1, 1.0f);
        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);

        program = GLES20.glCreateProgram();

        int vertex_shader = Shader.loadShader(GLES20.GL_VERTEX_SHADER, Shader.vertexShaderCode);
        int fragment_shader = Shader.loadShader(GLES20.GL_FRAGMENT_SHADER, Shader.fragmentShaderCode);

        GLES20.glAttachShader(program, vertex_shader);
        GLES20.glAttachShader(program, fragment_shader);
        GLES20.glLinkProgram(program);

        Presents = new ArrayList<>();

        float angle = 0;
        float x = cos(angle)*radius_x;//+center_x;
        float y = sin(angle)*radius_y;//+center_y;
        Presents.add(new PackagePresent(new Present(x, y, R.drawable.blue_present, activity), angle));
        angle = (float) Math.PI/3.5f;x = cos(angle)*radius_x; y = sin(angle)*radius_y;
        Presents.add(new PackagePresent(new Present(x, y, R.drawable.green_present, activity), angle));
        angle = (float)(2* Math.PI/3.5f);x = cos(angle)*radius_x; y = sin(angle)*radius_y;
        Presents.add(new PackagePresent(new Present(x, y, R.drawable.orange_present, activity), angle));
        angle = (float)(3* Math.PI/3.5f);x = cos(angle)*radius_x; y = sin(angle)*radius_y;
        Presents.add(new PackagePresent(new Present(x, y, R.drawable.purple_present, activity), angle));
        angle = (float)(4* Math.PI/3.5f);x = cos(angle)*radius_x; y = sin(angle)*radius_y;
        Presents.add(new PackagePresent(new Present(x, y, R.drawable.red_present, activity), angle));
        angle = (float)(5* Math.PI/3.5f);x = cos(angle)*radius_x; y = sin(angle)*radius_y;
        Presents.add(new PackagePresent(new Present(x, y, R.drawable.violet_present, activity), angle));
        angle = (float)(6* Math.PI/3.5f);x = cos(angle)*radius_x; y = sin(angle)*radius_y;
        Presents.add(new PackagePresent(new Present(x, y, R.drawable.yellow_present, activity), angle));

        for (PackagePresent p: Presents){
            p.p.ScalePresent(0.8f, 0.8f);
        }

        OpenedPresent = new Present(-1.3f,-2f, R.drawable.opened_present, activity);
        OpenedPresent.ScalePresent(2.6f, 2f);
        TextPresent = new Present(-1.9f,-0.2f,DisplayText,activity);
        TextPresent.ScalePresent(3.6f,2.8f);

        PresentName = new Present(-1.8f,-1.25f,name, activity);
        PresentName.ScalePresent(3.6f,2.5f);
        //PresentName.setDepth(1);
        Background = new Present(0,0, R.drawable.christmastree_background, activity);
        Background.setDepth(-1);
        GenThread();
        this.MovmentThread = new Thread(MovmentThreadCode);
        MovmentThread.start();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);

        float scale = 2.0f;
        float ratio = (float)width/height;

        Matrix.frustumM(ProjectionMatrix, 0, -ratio * scale, ratio * scale, -1 * scale, 1 * scale, 3, 7);

        Background.SetPresentForBackGround(ratio * 4.1f * scale, ratio * 7.2f * scale);//3.5,6 for 0 depth // 4.1,7.2 fo depth 1

    }

    @Override
    public void onDrawFrame(GL10 gl) {
        if(pause)
            pause = false;

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        GLES20.glUseProgram(program);

        Matrix.setLookAtM(ViewMatrix, 0,/*eye postion*/ 0f, 0f, 5f, /*center*/0f, 0f, 0f, /*up*/0f, 1f, 0f);
        Matrix.multiplyMM(MVPMatrix, 0, ProjectionMatrix, 0, ViewMatrix, 0);
        int ViewMatrix = GLES20.glGetUniformLocation(program, "View");
        GLES20.glUniformMatrix4fv(ViewMatrix, 1, false, MVPMatrix, 0);


        Shader.checkGlError("Draw");


        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glEnable(GLES20.GL_BLEND);


        switch (Current_Drawing)
        {
            case DrawPresent:
                Background.draw(program);
                for (PackagePresent p: Presents){
                    p.draw(program);
                }
                break;

            case BrightScreen:
                break;

            case DeadPresent:
                Background.draw(program);
                OpenedPresent.draw(program);
                OpenedPresent.ChangeAlpha(0.02f);
                PresentName.draw(program);
                TextPresent.draw(program);
                break;
        }

        GLES20.glDisable(GLES20.GL_BLEND);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

        stop = true;
        MovmentThread.join();
    }

    private void GenThread()
    {
        this.MovmentThreadCode = new Runnable() {
            @Override
            public void run() {
                while (!stop) {
                    if (pause) {
                        try {
                            Thread.sleep(waittime * 5);
                        } catch (Exception e) {}
                    }

                    if(Current_Drawing == Draw.BrightScreen)
                    {
                        slowDown++;
                        if(slowDown == 30)
                        {
                            Current_Drawing = Draw.DeadPresent;
                            break;
                        }
                    }
                    else
                    {
                        if (clicked == true) {
                            Log.d(TAG, "" + pace);
                            if (pace < -0.008)
                                pace += 0.0001;
                            else {
                                float wantedangleb = (float) (2 * Math.PI / 3.5f) - 0.2f;
                                for (PackagePresent p : Presents) {
                                    float ang = p.angle - wantedangleb*( p.angle / wantedangleb );
                                    if(ang < 0.0005 || ang > -0.0005)
                                        Current_Drawing = Draw.BrightScreen;

                                }
                            }
                        }

                        for (PackagePresent p : Presents) {
                            float x = cos(p.angle) * radius_x + center_x;
                            float y = sin(p.angle) * radius_y + center_y;
                            float z = sin(p.angle) * radius_z + center_z;
                            p.p.SetPresentLocation(x, y);
                            p.p.setDepth(z);
                            p.angle += pace;
                        }
                    }
                    try
                    {
                        Thread.sleep(waittime);
                    }
                    catch (Exception e) {}
                }
            }
        };
    }


    public void onPause()
    {
        pause = true;
    }

    public void onResume()
    {
        pause = false;
    }

    public void onStop()
    {
        stop = true;
        try {
            MovmentThread.join();
        }
        catch (Exception e)
        {

        }
    }

    public void onTouch()
    {
        clicked = true;
    }


    private float cos(float val)
    {
        return (float) Math.cos(val);
    }

    private float sin(float val)
    {
        return (float) Math.sin(val);
    }
}

