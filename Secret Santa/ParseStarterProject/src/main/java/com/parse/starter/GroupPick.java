/**
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
/**
 * Created by 100490371
 */
package com.parse.starter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;


public class GroupPick extends Activity implements ListView.OnItemClickListener {
    ListView santaList;
    String UserID;

    final String yourturn = "Your Turn!\nTap To Play.";
    final String yourassigned = "You're Assigned!\nTap To see who";
    final String groupcreation = "Party Is Being Created\nWait Untill The Group Is Ready";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.pick_party_activity);
        BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.background_stars);
        bitmap.setTileModeX(Shader.TileMode.REPEAT);
        findViewById(R.id.background).setBackground(bitmap);
        santaList = (ListView)findViewById(R.id.lstPeople);

        try
        {
            UserID = getIntent().getStringExtra("username");
        }
        catch (Exception e)
        {
            UserID = "";
        }
        santaList.setOnItemClickListener(this);
    }

    private void AddToList(ArrayList<PartyStatus> list)
    {
        santaList.setAdapter(new PartyAdapter(this, list));
    }


    //Callback function for findInBackground
    protected FindCallback FindParties = new FindCallback<SantaUserGroup>() {
        @Override
        public void done(List<SantaUserGroup> objects, ParseException e) {
            Log.d("Testing", "Go");
            if (e==null) {
                ArrayList<PartyStatus> parties = new ArrayList<>();
                for (SantaUserGroup pa: objects)
                {
                    String groupid =  pa.getGroup();
                    String GroupName = "";
                    String MyPerson = "";

                    ParseQuery<SantaGroup> q = SantaGroup.getQuery();
                    q.whereEqualTo("sharecode", groupid);
                    q.setLimit(1);


                    boolean InCreation = false;
                    String creator = "";
                    try {
                        List<SantaGroup> g = q.find();
                        GroupName = g.get(0).getname();
                        InCreation = g.get(0).getReady();
                        creator = g.get(0).getCreator();
                    } catch (ParseException e1) {e1.printStackTrace();}
                    MyPerson = pa.getMyPerson();



                    PartyStatus party;
                    if(!InCreation)
                        party = new PartyStatus(GroupName, groupcreation, Color.BLUE, groupid, MyPerson, InCreation, creator);
                    else if(MyPerson.equals(" "))
                        party = new PartyStatus(GroupName, yourturn, Color.RED, groupid, MyPerson, InCreation, creator);
                    else
                        party = new PartyStatus(GroupName, yourassigned, Color.GRAY, groupid, MyPerson, InCreation, creator);

                    parties.add(party);
                }
                AddToList(parties);
            }
            else {
                Toast.makeText(getApplicationContext(), "Error Loading, Back out and return to refresh. " + e, Toast.LENGTH_LONG).show();
            }
        }
    };






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
          // Handle action bar item clicks here. The action bar will
          // automatically handle clicks on the Home/Up button, so long
          // as you specify a parent activity in AndroidManifest.xml.
          int id = item.getItemId();

          //noinspection SimplifiableIfStatement
          if (id == R.id.action_settings) {
            return true;
          }

          return super.onOptionsItemSelected(item);
  }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        PartyStatus party = (PartyStatus)parent.getItemAtPosition(position);

        if(!party.getInCreation())
        {

            if(UserID.equals(party.getCreator())) {
                Intent getReady = new Intent(this, com.parse.starter.ViewGroup.class);

                getReady.putExtra("groupName", party.getPartyname());
                getReady.putExtra("groupCode", party.getPartyID());

                startActivity(getReady);
                //Toast.makeText(getApplicationContext(), "Rock And Roll", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Party hasn't started, Please wait", Toast.LENGTH_SHORT).show();
            }
        }
        else {

            Intent openPresent = new Intent(this, OpenPresent.class);

            if (!party.getMyPerson().equals(" ")) {
                openPresent.putExtra("IsPicked", true);
                openPresent.putExtra("MyPerson", party.getMyPerson());
            } else {
                openPresent.putExtra("IsPicked", false);
                openPresent.putExtra("username", UserID);
                openPresent.putExtra("party", party.getPartyID());
            }

            startActivity(openPresent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        santaList.setAdapter(null);
        ParseQuery query = new ParseQuery(SantaUserGroup.class);//SantaGroup.class);//SantaUser.class);//SantaUserGroup.class);//"User");//("UserGroup");
        query.whereEqualTo("username", UserID);

        query.findInBackground(FindParties);
    }
}

class PartyAdapter extends BaseAdapter
{
    private Context context;
    private ArrayList<PartyStatus> data;

    public PartyAdapter(Context context, ArrayList<PartyStatus> data) {
        this.data = data;
        this.context = context;
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        PartyStatus party = data.get(position);

        if (convertView == null) {
            // create the layout
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.santa_layout, parent, false);
        }

        // populate the views with the data from story
        TextView lblName = (TextView)convertView.findViewById(R.id.lblPartyName);
        lblName.setText(party.getPartyname());
        lblName.setTextColor(party.getColour());


        TextView lblstatus = (TextView)convertView.findViewById(R.id.lblPartyStatus);
        lblstatus.setText(party.getStatus());
        lblstatus.setTextColor(party.getColour());

        return convertView;
    }
}


class PartyStatus
{

    private String partyname;
    private String status;
    private String PartyID;
    private String MyPerson;
    private String creator;
    private int Colour;
    private boolean inCreation;

    public PartyStatus(String party, String status, int color, String PartyID, String MyPerson, boolean increation, String creater) {
        this.partyname = party;
        this.status = status;
        this.Colour = color;
        this.PartyID = PartyID;
        this.MyPerson = MyPerson;
        this.inCreation = increation;
        this.creator = creater;
    }

    public String getPartyname() {
        return partyname;
    }
    public String getStatus() {
        return status;
    }
    public int getColour() {
        return Colour;
    }
    public String getPartyID() {
        return PartyID;
    }
    public String getMyPerson() {
        return MyPerson;
    }
    public boolean getInCreation() {
        return inCreation;
    }
    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
    public void setInCreation(boolean inCreation) {
        this.inCreation = inCreation;
    }
    public void setMyPerson(String myPerson) {
        MyPerson = myPerson;
    }
    public void setPartyID(String partyID) {
        PartyID = partyID;
    }
    public void setColour(int colour) {
        Colour = colour;
    }
    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }
    public void setStatus(String status) {
        this.status = status;
    }


}
