/**
 * Created by 100490371 on 11/18/2015.
 */
package com.parse.starter;

import android.opengl.GLES20;
import android.opengl.GLU;
import android.util.Log;

class Shader
{

    private static final String TAG = "Testing";
    public static final String vertexShaderCode =
            "uniform mat4 View;" +
                    "uniform mat4 Location;" +
                    "attribute vec2 vTex;" +
                    "attribute vec4 vPosition;" +
                    "varying vec2 TexCoord;" +
                    "void main() {" +
                    "  gl_Position = (View *Location) * vPosition;" +
                    "  TexCoord = vTex;" +
                    "}";

    public static final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform sampler2D Tex;" +
                    "uniform float alpha;" +
                    "varying vec2 TexCoord;" +
                    "void main() {" +
                    "  vec4 color  = texture2D(Tex, TexCoord);" +
                    "  color.a += alpha;" +
                    "  gl_FragColor = color ;" +
                    "}";


    public  static int loadShader(int type, String code)
    {
        int shader = GLES20.glCreateShader(type);

        GLES20.glShaderSource(shader, code);
        GLES20.glCompileShader(shader);
        return shader;
    }

    public static void checkGlError(String glOperation) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e("Testing", glOperation + ": glError " + error + " GLuErrorString: " + GLU.gluErrorString(error));
            throw new RuntimeException(glOperation + ": glError " + error + " GLuErrorString: " + GLU.gluErrorString(error));
        }
    }

    public static int createAndLinkProgram(final int vertexShaderHandle, final int fragmentShaderHandle, final String[] attributes)
    {
        int programHandle = GLES20.glCreateProgram();

        if (programHandle != 0) {
            // Bind the vertex shader to the program.
            GLES20.glAttachShader(programHandle, vertexShaderHandle);

            // Bind the fragment shader to the program.
            GLES20.glAttachShader(programHandle, fragmentShaderHandle);

            // Bind attributes
            if (attributes != null)
            {
                final int size = attributes.length;
                for (int i = 0; i < size; i++)
                {
                    GLES20.glBindAttribLocation(programHandle, i, attributes[i]);
                }
            }

            // Link the two shaders together into a program.
            GLES20.glLinkProgram(programHandle);

            // Get the link status.
            final int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);

            // If the link failed, delete the program.
            if (linkStatus[0] == 0)
            {
                Log.e("Testing", "Error compiling program: " + GLES20.glGetProgramInfoLog(programHandle));
                GLES20.glDeleteProgram(programHandle);
                programHandle = 0;
            }
        }

        if (programHandle == 0)
        {
            throw new RuntimeException("Error creating program.");
        }

        return programHandle;
    }

}
