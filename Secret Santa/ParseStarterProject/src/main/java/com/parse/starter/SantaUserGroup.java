package com.parse.starter;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by 100490371 on 11/30/2015.
 */

@ParseClassName("UserGroup")
public class SantaUserGroup extends ParseObject {

    public String getUsername()
    {
        return getString("username");
    }

    public String getname()
    {
        return getString("name");
    }

    public String getGroup()
    {
        return getString("group");
    }

    public String getMyPerson()
    {
        return getString("myperson");
    }

    public boolean getSS()
    {
        return getBoolean("needSS");
    }
    public static ParseQuery<SantaUserGroup> getQuery()
    {
        return ParseQuery.getQuery(SantaUserGroup.class);
    }
}