package com.parse.starter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by 100485988 on 23-Nov-15.
 */
public class LabelAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> data;

    public LabelAdapter(Context context, ArrayList<String> data) {
        this.data = data;
        this.context = context;
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return data.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;

        if (convertView == null) {
            // if there is no object to reuse, create one
            textView = new TextView(context);
            textView.setText(data.get(position));
        } else {
            // we're reusing an old object
            textView = (TextView)convertView;
        }

        return textView;
    }
}
