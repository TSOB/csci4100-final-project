package com.parse.starter;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import java.io.IOException;

/**
 * Created by Nicholas Casbarro 100485988 on 19-Nov-15.
 */
public class Sound {

    private String path;
    private MediaPlayer player;
    private Context context;

    public Sound(Context c, String path) {
        this.path = path;
        this.context = c;
        try {
            AssetFileDescriptor afd = this.context.getAssets().openFd(this.path);
            this.player = new MediaPlayer();
            this.player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            afd.close();
            this.player.prepare();
        }
        catch (IOException e) {

        }
    }

    public void play(float vol) {
        this.player.start();
        this.player.setLooping(false);
        this.player.setVolume(vol, vol);
    }

    public void loop(float vol) {
        play(vol);
        this.player.setLooping(true);
    }

    public void stop() {
        this.player.pause();
        this.player.seekTo(0);
    }

    public void release() {
        this.player.release();
    }
}
