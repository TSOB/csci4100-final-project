/**
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
/**
 * Created by 100485988
 */
package com.parse.starter;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

//this only executes the first time the app is installed.
public class StarterApplication extends Application {

  ParseUser user;
  @Override
  public void onCreate() {
    super.onCreate();

    // Enable Local Datastore.
    Parse.enableLocalDatastore(this);

    // initialization code
    ParseObject.registerSubclass(SantaPerson.class);
    ParseObject.registerSubclass(SantaUserGroup.class);
    ParseObject.registerSubclass(SantaGroup.class);
    Parse.initialize(this, "SYf39ToMwHTJBSHYL47aFPDT50P0ikPFdrl7bCha", "t21JGQvznCfEZYKtrRYIM9y14CaRfcyUhR1DAtxX");
    ParseInstallation.getCurrentInstallation().saveInBackground();

    //callback for when the app tries to sign in/sign up.
    SignUpCallback internetCheck = new SignUpCallback() {
      @Override
      public void done(ParseException e) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("username", user.getUsername());
        installation.saveInBackground();
      }
    };

    //Check data from local storage to see if this user exists yet
    String uniqueName;
    try {
      FileInputStream fis = openFileInput("user_info.dat");
      uniqueName = String.valueOf(fis.read());
      fis.close();
    }
    catch (IOException e) {
      Log.d("No File: ","The save file does not exist.");
      uniqueName="";
    }
    if (uniqueName.equals("")) {
      //This is a new user, so sign up.

      user = new ParseUser();
      uniqueName = String.valueOf(System.nanoTime());
      user.setUsername(uniqueName);
      //Passwords not necessary for this app but are required by Parse
      user.setPassword("0");
      //TODO: set up display name. (Read from phone profile? Ask with popup?)
      final String[] SELF_PROJECTION = new String[]{ContactsContract.CommonDataKinds.Phone._ID,
              ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,};
      Cursor cursor = getContentResolver().query(
              ContactsContract.Profile.CONTENT_URI, SELF_PROJECTION, null, null, null);
      cursor.moveToFirst();

      String name = cursor.getString(1); //get the user's name from contacts
      user.put("name", name);
      ParseObject table = ParseObject.create("Person");//new ParseObject("Person");
      table.put("username", uniqueName); //code that identifies the group
      table.put("name", name); //user-friendly name of the group
      try {
        table.save();
      } catch (ParseException e) {
        e.printStackTrace();
      }
      Log.d("rawwwwwr", "it happened");

      try {
        user.signUpInBackground(internetCheck);
        FileOutputStream fos = openFileOutput("user_info.dat", Context.MODE_PRIVATE);
        fos.write(uniqueName.getBytes());
        fos.close();
      } catch (IOException e) {
        Log.d("SAVE FAILURE:", "Couldn't save the username locally.");
      }
    }
    //ParseUser.enableAutomaticUser();
    ParseACL defaultACL = new ParseACL();
    defaultACL.setPublicReadAccess(true);
    defaultACL.setPublicWriteAccess(true);
    // Optionally enable public read access.
    // defaultACL.setPublicReadAccess(true);
    ParseACL.setDefaultACL(defaultACL, true);
  }
}
