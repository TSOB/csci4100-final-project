package com.parse.starter;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

import java.util.ArrayList;

/**
 * Created by 100485988 on 17-Nov-15.
 */
public class CreateGroup extends AppCompatActivity {

    private String groupName;
    //the price limit is forced to be a nice value like $20, so no decimals.
    private int priceLimit;
    String creator, name, shareCode, smsMessage, errMessage;

    TextView created;
    Button invite;
    Button make;

    //Callback function for saveInBackground for the Create button
    protected SaveCallback saveCheck = new SaveCallback() {
        @Override
        public void done(ParseException e) {
            if (e==null) {
                Sound chime = new Sound(getApplicationContext(), "chime.wav");
                chime.play(1f);
                created.setText(getString(R.string.group_creation_successful));
                ((EditText) findViewById(R.id.edit_group_name)).setEnabled(false);
                ((EditText) findViewById(R.id.edit_price_limit)).setEnabled(false);
                created.setVisibility(View.VISIBLE);
                invite.setVisibility(View.VISIBLE);
            }
            else {
                Sound failed = new Sound(getApplicationContext(), "minor chime.wav");
                failed.play(1f);
                errMessage = getApplicationContext().getString(R.string.error_save_failed);
                Toast toast = Toast.makeText(getApplicationContext(), errMessage, Toast.LENGTH_SHORT);
                toast.show();
                make.setEnabled(true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_group);

        //set up views
        created = (TextView) findViewById(R.id.label_created);
        invite = (Button) findViewById(R.id.btn_invite);
        make = (Button) findViewById(R.id.btn_create);

        //set up strings and view reference variables
        creator = getIntent().getStringExtra("creator");
        name = getIntent().getStringExtra("name");
        shareCode = creator.substring(4, 10);
        smsMessage = getApplicationContext().getString(R.string.sms_message);
        errMessage = getApplicationContext().getString(R.string.error_field_missing);
        final TextView editGroupName = ((TextView) findViewById(R.id.edit_group_name));
        final TextView editPriceLimit = ((TextView) findViewById(R.id.edit_price_limit));

        //the Create button's listener
        make.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean ok = true;
                try {
                    //get the values to store on the server
                    groupName = String.valueOf(editGroupName.getText());
                    priceLimit = Integer.parseInt(String.valueOf(editPriceLimit.getText()));
                }
                catch (NumberFormatException e) {
                    ok = false;
                    editPriceLimit.requestFocus();
                }
                if (groupName.length() == 0) {
                    ok = false;
                    editGroupName.requestFocus();

                }

                if (ok) {
                    //store the values in the Group table
                    ParseObject table = new ParseObject("Group");
                    table.put("sharecode", shareCode); //code that identifies the group
                    table.put("name", groupName); //user-friendly name of the group
                    table.put("pricelimit", priceLimit); //integer
                    table.put("creator", creator); //specifies the creator (as self)
                    table.put("ready", false); //only the creator decides when to start the game.
                    table.saveInBackground();

                    //store the relational values in the UserGroup table
                    ParseObject relate = new ParseObject("UserGroup");
                    relate.put("username", creator);
                    relate.put("name", name);
                    relate.put("group", shareCode);
                    relate.put("needSS",true);
                    relate.put("myperson","");
                    relate.saveInBackground(saveCheck); //saveCheck is the callback function

                    created.setText(getString(R.string.creating));
                    make.setEnabled(false);
                }
                else {
                    Toast toast = Toast.makeText(getApplicationContext(), errMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });

        //the Invite button's listener
        invite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("sms_body", smsMessage+" "+shareCode);
                startActivity(smsIntent);
            }
        });

    }
}
