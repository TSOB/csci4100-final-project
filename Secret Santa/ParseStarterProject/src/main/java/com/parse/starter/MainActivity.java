/**
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
/**
 * Created by 100485988.
 */
package com.parse.starter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity {

    Context AlertRef = this;
    String uniqueName, displayName;
    boolean canProceed = false;
    Sound back;

    final float TextSize = 24.0f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        back = new Sound(getApplicationContext(), "coming to town.mid");

        BitmapDrawable bitmap = (BitmapDrawable) getResources().getDrawable(R.drawable.background_stars);
        bitmap.setTileModeX(Shader.TileMode.REPEAT);
        findViewById(R.id.background).setBackground(bitmap);

        //load user data from local storage to "log in"
        //(The file is written the first time the app is run)
        try {
            FileInputStream fis = openFileInput("user_info.dat");
            while (true) {
                char c = (char) fis.read();
                Log.d("c: ",""+(int)c);
                if (c==65535) {
                    //read function returns 65535 when the stream ends
                    break;
                }
                uniqueName += c;
            }
            fis.close();
            uniqueName = uniqueName.substring(4);
        }
        catch (IOException e) {
            Log.d("ERROR LOADING DATA: ","The save file is corrupted.");
            uniqueName="";
        }
        //TO TEST THIS APP, WE AUTOMATICALLY ASSIGN YOU A SPECIAL USERNAME CODE.
        // DELETE THIS LINE TO GO BACK TO NORMAL.
        uniqueName="2348344326233114";
        //sign in automatically
        ParseUser.logInInBackground(uniqueName, "0", new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                if (user != null) {
                    displayName = user.getString("name");
                    canProceed = true;
                } else {
//                    displayName = user.getString("name");
                    canProceed = true;
                }
            }
        });

        //onClick listeners
        Button createGroup = (Button) findViewById(R.id.btn_create_group);
        createGroup.setBackgroundColor(Color.argb(0, 0, 0, 0));
        createGroup.setTextColor(Color.WHITE);
        createGroup.setTextSize(TextSize);
        createGroup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (canProceed) {
                    Intent gotoCreate = new Intent(getApplicationContext(), CreateGroup.class);
                    gotoCreate.putExtra("creator", uniqueName);
                    gotoCreate.putExtra("name", displayName);
                    startActivity(gotoCreate);
                }
            }
        });
        Button yourGroups = (Button) findViewById(R.id.btn_your_groups);
        yourGroups.setBackgroundColor(Color.argb(0, 0, 0, 0));
        yourGroups.setTextColor(Color.WHITE);
        yourGroups.setTextSize(TextSize);
        yourGroups.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (canProceed) {
                    Intent gotoGroupPick = new Intent(getApplicationContext(), GroupPick.class);
                    gotoGroupPick.putExtra("username", uniqueName);
                    startActivity(gotoGroupPick);
                }
            }
        });

        Button joinGroups = (Button) findViewById(R.id.btn_join_groups);
        joinGroups.setBackgroundColor(Color.argb(0, 0, 0, 0));
        joinGroups.setTextColor(Color.WHITE);
        joinGroups.setTextSize(TextSize);
        joinGroups.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (canProceed) {
                    AlertDialog.Builder alert;
                    final EditText input;
                    alert = new AlertDialog.Builder(AlertRef);

                    alert.setTitle("Group Code");
                    alert.setMessage("Enter The Group Code:");

                    // Set an EditText view to get user input
                    input = new EditText(AlertRef);
                    input.setInputType(InputType.TYPE_CLASS_PHONE);
                    alert.setView(input);

                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            String value = input.getText().toString();

                            ParseQuery<SantaGroup> q = SantaGroup.getQuery();
                            q.whereEqualTo("sharecode", value);
                            q.setLimit(1);

                            try {
                                List<SantaGroup> list = q.find();
                                if (list.size() == 0) {
                                    Toast.makeText(getApplicationContext(), "This Group Code Doesn't Exist.", Toast.LENGTH_LONG).show();
                                    return;
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            ParseObject UserGroup = ParseObject.create("UserGroup");//new ParseObject("UserGroup");
                            UserGroup.put("username", uniqueName);
                            UserGroup.put("name", displayName);
                            UserGroup.put("needSS", true);
                            UserGroup.put("myperson", " ");
                            UserGroup.put("group", value);
                            UserGroup.saveInBackground();
                            Toast.makeText(getApplicationContext(), "You Have Been Added To The Party.", Toast.LENGTH_LONG).show();
                            Sound aria = new Sound(getApplicationContext(), "chime.wav");
                            aria.play(1);
                        }
                    });

                    alert.show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
          return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        back.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        back.loop(1.0f);
    }
}


