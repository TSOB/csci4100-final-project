package com.parse.starter;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by 100490371 on 11/30/2015.
 */
@ParseClassName("Group")
public class SantaGroup extends ParseObject {

    public String getCreator()
    {
        return getString("creator");
    }

    public String getname()
    {
        return getString("name");
    }

    public int getPriceLimit()
    {
        return getInt("pricelimit");
    }

    public String getShareCode()
    {
        return getString("sharecode");
    }

    public boolean getReady()
    {
        return getBoolean("ready");
    }

    public static ParseQuery<SantaGroup> getQuery()
    {
        return ParseQuery.getQuery(SantaGroup.class);
    }
}