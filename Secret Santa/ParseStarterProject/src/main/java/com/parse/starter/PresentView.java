/**
 * Created by 100490371.
 */
package com.parse.starter;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

class PresentView extends GLSurfaceView
{
    private final PresentRender render;
    Context con;

    public PresentView(Context context, String Name, boolean picked)
    {
        super(context);
        con = context;
        setEGLContextClientVersion(2);

        render = new PresentRender(context, Name, picked);
        setRenderer(render);

        //setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

    }

    public void onStop()
    {
        render.onStop();
    }

    public void onPause()
    {
        render.onPause();
    }

    public void onResume()
    {
        render.onResume();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        render.onTouch();
        return true;
    }

}
