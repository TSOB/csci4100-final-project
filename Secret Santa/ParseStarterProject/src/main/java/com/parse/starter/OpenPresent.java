package com.parse.starter;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TimeUtils;
import android.view.WindowManager;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Created by 100490371 on 11/18/2015.
 */
public class OpenPresent extends Activity {
    String userID;
    String partyID;
    String pickedID;

    String PickedName;


    private PresentView display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent party = getIntent();


        boolean picked = party.getBooleanExtra("IsPicked", false);

        if(picked)
        {
            pickedID = party.getStringExtra("MyPerson");
            Log.d("Testing",""+ pickedID);
            ParseQuery<SantaPerson> query = SantaPerson.getQuery();
            query.whereEqualTo("username", pickedID);
            query.setLimit(1);

            try {
                List<SantaPerson> results = query.find();
                PickedName = results.get(0).getname();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
        {
            userID = party.getStringExtra("username");
            partyID = party.getStringExtra("party");

            Log.d("Testing",""+ userID);
            Log.d("Testing",""+ partyID);
            long seed = System.currentTimeMillis();
            Random rand = new Random(seed);
            try {
                ParseQuery query = new ParseQuery(SantaUserGroup.class);
                query.whereNotEqualTo("username", userID);
                query.whereEqualTo("needSS", true);
                query.whereEqualTo("group", partyID);

                List<SantaUserGroup> results = query.find();
                String temp = " ";
                for(int cnt = 0; cnt < results.size(); cnt++)
                    temp += results.get(cnt).getUsername() + " ";
                Log.d("Testing",""+ results.size());
                Log.d("Testing",""+ temp);

                int number = rand.nextInt(results.size());
                Log.d("Testing",""+ number);

                SantaUserGroup person = results.get(number);
                pickedID = person.getUsername();
                PickedName = person.getname();
                person.put("needSS", false);
                person.saveInBackground();

                Log.d("Testing", "" + pickedID);

                query = SantaUserGroup.getQuery();
                query.whereEqualTo("username", userID);
                query.setLimit(1);
                results = query.find();

                person = results.get(0);
                Log.d("Testing",""+ pickedID);
                person.put("myperson", pickedID);
                person.saveInBackground();


//
// Retrieve the object by id
//            query = SantaUserGroup.getQuery();
//            query.getInBackground(person.getObjectId(), new GetCallback<SantaGroup>() {
//                public void done(SantaUserGroup Party, ParseException e) {
//                    if (e == null) {
//                        // Now let's update it with some new data. In this case, only cheatMode and score
//                        // will get sent to the Parse Cloud. playerName hasn't changed.
//                        Party.put("needSS", true);
//                    }
//                }
//            });
//                PickedID = (String) picked.get("personID");
//
//                query = ParseQuery.getQuery("Persons");
//                query.whereNotEqualTo("personID", PickedID);
//
//                results = query.find();
//
//                PickedName = (String) results.get(0).get("name");
//                display = new PresentView(this, getIntent().getStringExtra("name"));

            } catch (ParseException e) {
                e.printStackTrace();
                Toast.makeText(this, "Unable to connect to the database", Toast.LENGTH_LONG).show();
                finish();
            }
        }


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        display = new PresentView(this, PickedName, picked);
        setContentView(display);


    }

    @Override
    protected void onPause() {
        super.onPause();
        display.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TESTING", "ONRESUME IS CALLED");
        display.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        display.onStop();
    }
}
